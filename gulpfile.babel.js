/**
 *  redcoon's Gulp tasker configuration
 */
import gulp from 'gulp';
import loadTasks from 'gulp-task-loader';
import path from 'path';

/**
 * Load tasks from gulp/tasks directory using gulp-task-loader.
 * @see https://github.com/hontas/gulp-task-loader
 */
loadTasks(path.join('gulp', 'tasks'));
/**
 *  Task for building entire project.
 */
gulp.task( 'build', [
    'templates',
    'fonts',
    'json',
    'scripts:components',
    'scripts:vendors',
    'scripts:standalone',
    'sprites:png',
    'images',
    'sprites:svg'
], function() {
    return gulp.start( 'styles:compile' );
} );

/**
 *  Task for cleaning and building entire project.
 */
gulp.task( 'compile', [ 'clean' ], function() {
    return gulp.start( 'build' );
} );

/**
 *  Task for project linting.
 */
gulp.task( 'lint', [ 'scripts:lint', 'styles:lint' ] );

/**
 *  Task that fires project linting on every commit attempt.
 */
gulp.task( 'pre-commit', [ 'lint' ] );

/**
 *  Default task
 */
gulp.task( 'default', [ 'compile' ] );
