<?php
/**
 * Display all php errors in case our system is malfunctioning.
 */
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
set_include_path(get_include_path() . PATH_SEPARATOR . realpath(__DIR__ . '/../'));

/**
 * Define device specific global variables.
 */
global $device, $mobile, $tablet, $desktop;
$mobile = $tablet = $desktop = false;
if (isset($_GET['tablet'])) {
    $tablet = true;
} elseif(isset($_GET['desktop'])) {
    $desktop = true;
} else {
    $mobile = true;
}
$device = $mobile ? 'mobile' : ($tablet ? 'tablet' : 'desktop');

/**
 * Load utilities.
 */
require 'utilities/_load.php';
require 'utilities/_common.php';
require 'utilities/_component.php';
require 'utilities/_layout.php';
?>
