<?php
/**
 * Simple function for flexible component loading.
 * It is possible to specify additional arguments as array which will be
 * accessible as variables inside component.
 *
 * @param  string $name Name of the component without underscore and extension.
 * @param  array $args Additional arguments that will be accessible as variables inside component.
 * @return null
 */
function load($directory, $name, array $args = []) {
    global $device, $mobile, $tablet, $desktop;
    /**
     * Default component variables.
     * @var array
     */
    $args = array_merge([
        /**
         * Additional classes that should be added to main component element.
         * E.g. <div class="header <?=$class;?>">...
         */
        'class'     => '',
        /**
         * Type of the component that you want to include from the file.
         */
        'type'      => ''
    ], $args);

    /**
     * Changing array to variables.
     */
    extract($args);
    $pathParts = explode('/', $name);
    $pathPartsLength = count($pathParts);
    $path = '';
    for($i = 0; $i < $pathPartsLength; $i++) {
        if($i === $pathPartsLength - 1) {
            $path .= '_' . $pathParts[$i] . '.php';
        } else {
            $path .= $pathParts[$i] . '/';
        }
    }
    echo PHP_EOL . PHP_EOL . PHP_EOL  . '<!--##### ' . $directory . $path . ' start #####-->' . PHP_EOL;
    include $directory . $path;
    echo '<!--##### ' . $directory . $path . ' end #####-->' . PHP_EOL . PHP_EOL . PHP_EOL;
}
