<?php
/**
 * Simple function for flexible component loading.
 * It is possible to specify additional arguments as array which will be
 * accessible as variables inside component.
 *
 * @param  string $name Name of the component without underscore and extension.
 * @param  array $args Additional arguments that will be accessible as variables inside component.
 * @return null
 */
function common($name, array $args = []) {
    global $device, $mobile, $tablet, $desktop;
    load('commons/', $name, $args);
}
