<!-- Including all of the JS scripts -->
<script src="/vendors/modernizr.js"></script>
<!-- Load jQuery from external CDN with chance that visitor already have it cached -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- If load from CDN failed, load local version instead -->
<script>window.jQuery || document.write('<script src="/vendors/jquery.js"><\/script>')</script>
<script src="/vendors.js"></script>
<script src="/components.js"></script>
