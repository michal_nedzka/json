import $ from 'jQuery';

/**
 * Global module that holds general utilities.
 */

var $window = $( window );

/**
 * Global responsive breakpoints.
 * @type {Object} Responsive breakpoints.
 */
var breakpoint = {

    //End point of breakpoint
    xsmall: 379,
    small: 767,
    medium: 1024,
    large: 1440,
    xlarge: 1920
};

/**
 * Tells if user is using a mobile device.
 * @return {Boolean} True if user is using mobile device, false otherwise.
 */
var isMobile = function() {
    var mobRegEx = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i;

    return mobRegEx.test( navigator.userAgent );
};

/**
 * Returns current orientation of user's device.
 * @return {String} 'landscape' if window width > height, 'portrait' otherwise.
 */
var orientation = function() {
    return $window.width() > $window.height() ? 'landscape' : 'portrait';
};

/**
 * Tells if user is using Internet Explorer
 * @return {Boolean} True if user is using IE, false otherwise.
 */
var isIE = function() {
    var ua = window.navigator.userAgent;

    return ua.match( /MSIE/ ) || ua.match( /Trident.*rv[ :]*11\./ );
};

/**
 * Returns android version if user is using Android device based on user agent string.
 * @param  {String} ua User agent string to parse.
 * @return {Number|Boolean} Android version or false if user is not on Android device.
 */
var getAndroidVersion = function( ua ) {
    ua = ( ua || navigator.userAgent ).toLowerCase();
    var match = ua.match( /android\s([0-9\.]*)/ );
    return match ? match[ 1 ] : false;
};

/**
 * Returns Android version or NaN if user not on Android.
 * @return {Number|NaN} ndroid version or NaN if user not on Android
 */
var androidVersion = function() {
    var version = parseFloat( getAndroidVersion() ); //4.2

    return version;
};

/**
 * Returns IE version if user is using IE based on user agent string.
 * @param  {String} ua User agent string to parse.
 * @return {Number|Boolean} IE version or false if user is not on IE
 */
var ieVersion = function() {
    var myNav = navigator.userAgent.toLowerCase();

    return ( myNav.indexOf( 'msie' ) !== -1 ) ? parseInt( myNav.split( 'msie' )[ 1 ], 10 ) : false;
};

export { breakpoint, isMobile, orientation, isIE, androidVersion, ieVersion };
