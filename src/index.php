<?php
set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
require '_setup.php';
common('head', ['title' => 'Pergenta - Prototype Printexpose']);
?>
    <body>
        <div class="pg-container">
            <div class="pg-container__wrapper">
                <div class="pg-section">
                    <?php component('page/page'); ?>
                </div>
                <div class="pg-section pg-section--right">
                    <?php component('button-container/button-container'); ?>
                    <?php component('header/header'); ?>
                </div>
            </div>
        </div>

        <?php common('scripts'); ?>
    </body>
</html>
