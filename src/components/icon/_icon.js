import $ from 'jQuery';

function openPrintOption() {

    let pdfWrapper = document.getElementsByClassName( 'pg-icon--pdf' )[0];

    $( pdfWrapper ).on( 'click', () => {
        window.print();
    } );
}

function handleNewPrototype() {

    let newIcon = document.getElementsByClassName('pg-icon--new')[0];

    $( newIcon ).on( 'click', () => {
        window.location.reload(true);
        $( window ).scrollTop(0);
    } );
}

function handleSvgChange( element, svg , newIcon ) {

    $( element ).on( 'click', () => {
        $( element ).children( svg ).find( 'use' ).attr( 'xlink:href', 'images/sprites.svg#' +  newIcon );
    } )
}

function showHideElement( handler, element, newClass, child ) {

    $( handler ).on( 'click', () => {

        $( element ).toggleClass( newClass );
        $( handler ).children( child ).toggleClass( 'pg-icon__image--active' );
    } );
}

function showHidePhoto() {

    let photoIcon = document.getElementsByClassName( 'pg-icon--photo' )[0];
    let photoContainer = document.getElementsByClassName( 'pg-page__image-wrapper--profile' )[0];
    let newClass = 'pg-page__image-wrapper--active';
    let child = document.getElementsByClassName( 'pg-icon__image' );

    showHideElement( photoIcon, photoContainer, newClass, child );
}

function countPages() {

    let pages = document.querySelectorAll( '.pg-page' );
    let page = document.getElementsByClassName( 'pg-page' );


    for ( let i = 1; i < pages.length + 1; i++ ) {
        let counter = $(page).find( '.pg-page__footer-counter' );

        $( counter ).each( (i, el) => {

            let $el = $(el);
            let newString = 'Seite ' + (i + 1) + '/' +  pages.length;
            $el.html(newString);

        })
    }
}

function handlePageCounter() {

    let counterIcon = document.getElementsByClassName( 'pg-icon--numerable' )[0];
    let counter = document.getElementsByClassName( 'pg-page__footer-counter' );
    let newClass = 'pg-page__footer-counter--active';
    let child = document.getElementsByClassName( 'pg-icon__image' );

    showHideElement( counterIcon, counter, newClass, child );
    countPages();
}

function handleSignature() {

    let signatureIcon = document.getElementsByClassName( 'pg-icon--signature' )[0];
    let child = document.getElementsByClassName( 'pg-icon__image' );

    let hiddenPage = document.getElementsByClassName('pg-page--hidden')[0];
    const cloneNode = $(hiddenPage).clone();
    $(hiddenPage).detach();

    showHideElement( signatureIcon, '' , '' , child );

    $( signatureIcon ).on( 'click', ( e ) => {

        e.preventDefault();
        let newClone =  cloneNode.clone();
        let page = $('.pg-page:nth-last-child(1)');

        let newPage = $('.pg-page:nth-last-child(2)');
        $('.pg-page--hidden').remove();

        if( !newPage.hasClass( 'pg-page--hidden' ) ) {
            page.before(newClone);
        }

        countPages();
    } )
}

function handleChangeNode() {

    let listener = ( e ) => {

        countPages();
    };

    let editable = document.querySelectorAll( '[contenteditable]' );

    editable.forEach( ( el ) => {

        el.addEventListener( 'input', listener, false );
        el.addEventListener( 'DOMNodeInserted', listener, false );
        el.addEventListener( 'DOMNodeInsertedIntoDocument', listener, false );
        el.addEventListener( 'DOMSubtreeModified', listener, false );
        el.addEventListener( 'DOMNodeRemoved', listener, false );
        el.addEventListener( 'DOMNodeRemovedFromDocument', listener, false );
        el.addEventListener( 'DOMCharacterDataModified', listener, false );
    } );
}

function moveElementOnScroll() {

    let menuBar = document.getElementsByClassName( 'pg-section--right' )[0];
    let heightHeader = $( '.pg-header' ).height();

    $( window ).scroll( () => {

        $( menuBar ).stop().animate(
            {
                'marginTop': ( $( window ).scrollTop() + 20  ) + 'px'
            },
        'slow' );
    } );

}

function handleInputFile() {
    let input = document.getElementsByClassName( 'pg-input--file' )[0];
    let wrapper = document.getElementsByClassName( 'pg-icon--attach' )[0];

    $( wrapper ).on( 'click', () => {
        let innerP = $( wrapper ).find('p');
        let innerHtml = innerP.html();
        innerP.html( 'weitere ' + innerHtml );
    } );

}


function handleActiveTemplate() {
    let template = document.getElementsByClassName( 'pg-prototypes__item' );

    $( template ).each( ( i, el ) => {

        let $el = $( el );
        $( template ).eq(0).addClass( 'pg-prototypes__item--active' );

        $el.on( 'click', ( e ) => {
            e.preventDefault();
            // $( template ).removeClass( 'pg-prototypes__item--active' );
            // $el.addClass( 'pg-prototypes__item--active' );
        } )

    } );

}

const init = function () {

    openPrintOption();
    handleSignature();
    handlePageCounter();
    showHidePhoto();
    handleNewPrototype();
    handleChangeNode();
    moveElementOnScroll();
    handleInputFile();
    handleActiveTemplate();
};

export { init };
