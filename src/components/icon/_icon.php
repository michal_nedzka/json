<div class="pg-icon">
    <div class="pg-icon__wrapper <?=$mod?>">
        <?php if($input): ?>
            <input class="pg-input pg-input--<?=$input?>" type="<?=$input?>">
        <?php endif; ?>
        <svg class="pg-icon__image <?=$modIcon?>">
            <use  xlink:href="images/sprites.svg#<?=$icon?>"></use>
        </svg>
        <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
            <?=$text?>
        </p>
    </div>
</div>