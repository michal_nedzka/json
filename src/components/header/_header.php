<div class="pg-header">
    <div class="pg-header__wrapper">
        <div class="pg-header__section">
            <?php component('icon/icon', ['icon' => 'new-file', 'text' => 'Daten zurücksetzen', 'mod' => 'pg-icon--new' ]); ?>
            <?php component('icon/icon', ['icon' => 'pdf', 'text' => 'PDF herunterladen', 'mod' => 'pg-icon--pdf' ]); ?>
        </div>
        <div class="pg-header__section pg-header__section--width_40">
            <?php component('icon/icon', ['icon' => 'attach', 'text' => 'Anlage hinzufüngen', 'input' => 'file', 'mod' => 'pg-icon--attach']); ?>
        </div>
        <div class="pg-header__section pg-header__section--column pg-header__section--width_60">
            <?php component('icon/icon', ['icon' => 'user', 'text' => 'Profilfoto', 'mod' => 'pg-icon__wrapper--row pg-icon--photo', 'modIcon' => 'pg-icon__image--small' ]); ?>
            <?php component('icon/icon', ['icon' => 'number', 'text' => 'Seitenzahlen', 'mod' => 'pg-icon__wrapper--row pg-icon--numerable', 'modIcon' => 'pg-icon__image--small' ]); ?>
            <?php component('icon/icon', ['icon' => 'anonymous', 'text' => 'Anonymisieren', 'mod' => 'pg-icon__wrapper--row pg-icon--anonymous', 'modIcon' => 'pg-icon__image--small' ]); ?>
<!--            --><?php /*component('icon/icon', ['icon' => 'signature', 'text' => 'Unterschrift', 'mod' => 'pg-icon__wrapper--row pg-icon--signature', 'modIcon' => 'pg-icon__image--small' ]); */?>

        </div>
        <div class="pg-header__section">
            <?php component('prototypes/prototypes'); ?>
        </div>
    </div>
</div>