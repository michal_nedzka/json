import $ from 'jQuery';


function setAttributes( el, options ) {

    Object.keys( options ).forEach( (attr) => {
        el.setAttribute( attr, options[ attr ] );
    } )
}

function createTableRow( element ) {

    let newTr =  document.createElement( 'tr' );
    newTr.setAttribute( 'class', 'pg-table__row' );
    let newTd = document.createElement( 'td' );
    setAttributes( newTd, { 'class': 'pg-table__row-item', 'contenteditable': 'true' } );

    newTr.appendChild( newTd );
    element[0].appendChild( newTr );
}

function createDoubleTableRow( element ) {

    let newTr =  document.createElement( 'tr' );
    newTr.setAttribute( 'class', 'pg-table__row' );
    let newTd = document.createElement( 'td' );
    setAttributes( newTd, { 'class': 'pg-table__row-item', 'contenteditable': 'true' } );

    newTr.appendChild( newTd );
    let cloneTd = newTd.cloneNode();
    newTr.appendChild( cloneTd );
    element[0].appendChild( newTr );
}

function handleTableRows() {

    let addControl = document.getElementsByClassName( 'pg-table__controls-add' );
    let removeControl = document.getElementsByClassName( 'pg-table__controls-remove' );

    $( addControl ).each( ( i, el ) => {
        let $e = $( el );

        $e.on( ' click', () => {
            let table = $e.parent().parent();
            let thisTable = $e.parent().next().children();

            if ( $( table ).hasClass( 'pg-table--simple' ) ) {
                createTableRow( thisTable )
            } else {
                createDoubleTableRow( thisTable );
            }
        } )
    } );

    $( removeControl ).each( ( i, el ) => {
        let $e = $( el );

        $e.on( ' click', () => {
            let thisTable = $e.parent().next();
            let lastNode = $( thisTable ).children().children().last();
            lastNode.remove();
        } )
    } );

}

function showArrowControls ( element ) {

    let arrowContainer = document.createElement('div');
    arrowContainer.setAttribute( 'class', 'pg-page__section-controls');
    let arrowUp = document.createElement('div');
    arrowUp.setAttribute( 'class', 'pg-control pg-page__section-controls-up pg-control--transformed');
    let arrowDown = document.createElement('div');
    arrowDown.setAttribute( 'class', 'pg-control pg-page__section-controls-down');

    let iconUp = document.createElement('div');
    iconUp.setAttribute( 'class', 'pg-control pg-page__section-controls-up');
    let iconDown = document.createElement('div');
    iconDown.setAttribute( 'class', 'pg-control pg-page__section-controls-down');

    let innerIcon = document.createElement( 'span' );
    innerIcon.setAttribute( 'class', 'pg-control__icon-arrow');

    let innerIcon2 = document.createElement( 'span' );
    innerIcon2.setAttribute( 'class', 'pg-control__icon-arrow');

    iconUp.appendChild( innerIcon );
    iconDown.appendChild( innerIcon2 );

    arrowUp.appendChild( iconUp );
    arrowDown.appendChild( iconDown );


    arrowContainer.appendChild( arrowUp );
    arrowContainer.appendChild( arrowDown );
    arrowContainer.addEventListener( 'mouseover', (e) => { e.preventDefault(); e.stopPropagation(); });

    element.appendChild( arrowContainer );

}

function removeControls( element ) {

    let controls = $( element ).find( '.pg-page__section-controls' );
    controls.remove()
}

function handleArrowUpControl( arrowUp, elBefore,  element ) {

    let parent = element.parentNode;
    let cloneNode = $( element ).clone()[0];
    element.remove();
    parent.insertBefore( cloneNode, elBefore  );

}

function handleSwitchableAreas() {

    let handler = document.querySelectorAll( '.pg-page__section' );


    handler.forEach( ( el ) => {

        let elBefore = el.previousElementSibling;
        let elAfter = el.nextElementSibling;

        el.addEventListener( 'mouseover', ( e ) => {
            e.preventDefault();
            e.stopPropagation();

            showArrowControls( el );
            el.setAttribute( 'style', 'background: #F7F7F7; cursor: pointer; ');

            let arrows = $( el ).find( '.pg-page__section-controls' );
            let arrowUp =  $( arrows ).find( '.pg-page__section-controls-up' );
            let arrowDown =  $( arrows ).find( '.pg-page__section-controls-down' );

            //arrowUp.addEventListener( 'click', handleArrowUpControl( arrowUp, elBefore, el ), true);

            el.addEventListener( 'mouseout', ( e ) => {
                e.preventDefault();
                e.stopPropagation();
                removeControls( el );
                el.removeAttribute( 'style' );
           }, false );
        }, false );


    } );
}

const init = function () {

    handleTableRows();
    //handleSwitchableAreas();
};

export { init };