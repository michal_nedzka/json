<ul class="pg-list">
    <?php foreach ($listItems as $listItem): ?>
        <li class="pg-list__item">
            <svg class="pg-list__icon tr-link__icon">
                <use  xlink:href="images/sprites.svg#<?=$listItem['icon']?>"></use>
            </svg>
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_2" contenteditable="true">
                <?=$listItem['text']?></p>
        </li>
    <?php endforeach; ?>
</ul>