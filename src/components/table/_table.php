<?php if($simple): ?>
    <div class="pg-table pg-table--simple">
        <div class="pg-table__controls">
            <?php component('control/control', ['mod' => 'pg-table__controls-add', 'icon' => 'add']); ?>
            <?php component('control/control', ['mod' => 'pg-table__controls-remove', 'icon' => 'remove']); ?>
        </div>
        <table class="pg-table__wrapper">
            <tr class="pg-table__row">
                <td class="pg-table__row-item" contenteditable="true">
                </td>
            </tr>
            <tr class="pg-table__row">
                <td class="pg-table__row-item" contenteditable="true"></td>
            </tr>
            <tr class="pg-table__row">
                <td class="pg-table__row-item" contenteditable="true"></td>
            </tr>
        </table>
    </div>
<?php else: ?>
    <div class="pg-table">
        <div class="pg-table__controls">
            <?php component('control/control', ['mod' => 'pg-table__controls-add', 'icon' => 'add']); ?>
            <?php component('control/control', ['mod' => 'pg-table__controls-remove', 'icon' => 'remove']); ?>
        </div>
        <table class="pg-table__wrapper">
            <tr class="pg-table__row">
                <td class="pg-table__row-item" contenteditable="true" >
                    von – bis
                </td>
                <td class="pg-table__row-item" contenteditable="true" >
                    <?=$tableOption ?>
                </td>
            </tr>
            <tr class="pg-table__row">
                <td class="pg-table__row-item" contenteditable="true"></td>
                <td class="pg-table__row-item" contenteditable="true"></td>
            </tr>
            <tr class="pg-table__row">
                <td class="pg-table__row-item" contenteditable="true"></td>
                <td class="pg-table__row-item" contenteditable="true"></td>
            </tr>
            <tr class="pg-table__row">
                <td class="pg-table__row-item" contenteditable="true"></td>
                <td class="pg-table__row-item" contenteditable="true"></td>
            </tr>
        </table>
    </div>
<?php endif; ?>
