<div class="pg-prototypes">
    <div class="pg-prototypes__wrapper swiper-container">
        <div class="pg-prototypes__arrow swiper-button-prev">
            <svg class="pg-prototypes__arrow-icon pg-prototypes__arrow-icon--top">
                <use  xlink:href="images/sprites.svg#arrow"></use>
            </svg>
        </div>
        <div class="pg-prototypes__content swiper-wrapper">
            <div class="pg-prototypes__item swiper-slide">
                <img class="pg-prototypes__icon" src="/images/1-tempton.png" alt="tempton">

                <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
                    Tempton Template
                </p>
            </div>
            <div class="pg-prototypes__item swiper-slide">
                <img class="pg-prototypes__icon" src="/images/2-tempton.png" alt="tempton">
                <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
                    Default Template
                </p>
            </div>
            <div class="pg-prototypes__item swiper-slide">
                <img class="pg-prototypes__icon" src="/images/1-tempton.png" alt="tempton">

                <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
                    Tempton Template
                </p>
            </div>
            <div class="pg-prototypes__item swiper-slide">
                <img class="pg-prototypes__icon" src="/images/2-tempton.png" alt="tempton">
                <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
                    Default Template
                </p>
            </div>
            <div class="pg-prototypes__item swiper-slide">
                <img class="pg-prototypes__icon" src="/images/1-tempton.png" alt="tempton">

                <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
                    Tempton Template
                </p>
            </div>
            <div class="pg-prototypes__item swiper-slide">
                <img class="pg-prototypes__icon" src="/images/2-tempton.png" alt="tempton">
                <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
                    Default Template
                </p>
            </div>
        </div>
        <div class="pg-prototypes__arrow swiper-button-next">
            <svg class="pg-prototypes__arrow-icon">
                <use  xlink:href="images/sprites.svg#arrow"></use>
            </svg>
        </div>
    </div>
</div>
