import $ from 'jQuery';

function initSwiper() {
    return new Swiper( '.pg-prototypes__wrapper', {
        direction: 'vertical',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 3
    } );
}

const init = function () {
    initSwiper();
};

export { init };