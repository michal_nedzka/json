<div class="pg-page">
    <div class="pg-page__image-wrapper pg-page__image-wrapper--top">
        <img class="pg-page__image pg-page__image--logo" src="/images/logo.png" alt="tempton">
    </div>

    <div class="pg-page__section pg-page__section--width_50">
        <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">kontaktdaten</h2>
        <div class="pg-page__section-wrapper">
            <p class="pg-paragraph pg-paragraph--bold pg-paragraph--color_black" contenteditable="true">TEMPTON – Frau Anja Gschwind</p>
            <p class="pg-paragraph pg-paragraph--bold pg-paragraph--color_black" contenteditable="true">Tempton München, Schillerstraße 30</p>
        </div>
        <div class="pg-page__section-wrapper">
            <p class="pg-paragraph pg-paragraph--color_black" contenteditable="true">+49 89 750754-639</p>
            <p class="pg-paragraph pg-paragraph--color_black" contenteditable="true">E-Mail-Adresse</p>
        </div>
    </div>
    <div class="pg-page__section">
        <h2 class="pg-headline pg-headline--size_2 pg-headline--color_turq pg-headline--uppercase" contenteditable="true">personalprofil</h2>
    </div>
    <div class="pg-page__section">
<!--        <div class="pg-page__section-controls">-->
<!--            --><?php //component('control/control', ['mod' => 'pg-page__section-controls-up pg-control--transformed', 'icon' => 'arrow-down']); ?>
<!--            --><?php //component('control/control', ['mod' => 'pg-page__section-controls-down', 'icon' => 'arrow-down']); ?>
<!--        </div>-->
        <div class="pg-page__section-wrapper pg-page__section-wrapper--row">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">wir stellen vor
            </h2>
            <h2 class="pg-headline" contenteditable="true">Qualifikation
            </h2>
        </div>
        <div class="pg-page__section-wrapper">
            <form class="pg-form">
                <label class="pg-form__label" for="date" contenteditable="true">Verfügbar ab</label>
                <input class="pg-form__input" type="date" value="2017-06-01" id="date" min="1900-01-01"  contenteditable="true">
            </form>
            <form class="pg-form">
                <label class="pg-form__label" for="Einsatzort" contenteditable="true">Einsatzort</label>
                <input class="pg-form__input" type="text" id="Einsatzort"  value="München" contenteditable="true">
            </form>
        </div>
    </div>
    <div class="pg-page__section">

        <div class="pg-page__section-wrapper pg-page__section-wrapper--margin_0">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">PERSÖNLICHE DATEN
            </h2>
            <form class="pg-form">
                <label class="pg-form__label" for="name" contenteditable="true">Name, Vorname</label>
                <input class="pg-form__input" type="text" value="Bergmann, Konrad" id="name"  contenteditable="true">
            </form>
            <form class="pg-form">
                <label class="pg-form__label" for="city" contenteditable="true">Wohnort</label>
                <input class="pg-form__input" type="text" id="city" value="Dresden" contenteditable="true">
            </form>
        </div>
        <div class="pg-page__section-wrapper pg-page__section-wrapper--row pg-page__section-wrapper--width_60">
            <form class="pg-form">
                <label class="pg-form__label" for="birth" contenteditable="true">Geboren</label>
                <input class="pg-form__input" type="text" id="birth"  contenteditable="true">
            </form>
            <form class="pg-form">
                <label class="pg-form__label" for="nation" contenteditable="true">Nationalität</label>
                <input class="pg-form__input" type="text" id="nation"  contenteditable="true">
            </form>
        </div>
        <div class="pg-page__image-wrapper pg-page__image-wrapper--profile">
            <img class="pg-page__image pg-page__image--profile" src="/images/user.png" alt="tempton">
        </div>
    </div>
    <div class="pg-page__section">
        <div class="pg-page__section-wrapper">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">
                GUTE GRÜNDE FÜR UNSERE EMPFEHLUNG
            </h2>
            <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black" contenteditable="true">
                Herr xy macht einen sehr guten Eindruck... und stellt sich gerne bei Ihnen vor.
            </p>
        </div>
    </div>
    <div class="pg-page__section">
        <div class="pg-page__section-wrapper">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">
                AUSBILDUNG / WEITERBILDUNG
            </h2>
            <?php component('table/table', ['tableOption' => 'Aus-/Weiterbildungsstätte']); ?>
        </div>
    </div>
    <div class="pg-page__section">
        <h2 class="pg-headline pg-headline--color_turq" contenteditable="true">
            Die für Sie interessante Berufserfahrung unseres Besetzungsvorschlages sehen Sie auf der nächsten Seite.
        </h2>
    </div>
    <div class="pg-page__section pg-page__section--footer">
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                200000_V1_11.02.2017
            </p>
        </div>
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                Alle personenbezogenen Bezeichungen stehen fur beide Geschlechter
            </p>
        </div>
        <div class="pg-page__footer-counter"></div>
    </div>
</div>

<div class="pg-page">
    <div class="pg-page__image-wrapper pg-page__image-wrapper--top">
        <img class="pg-page__image pg-page__image--logo" src="/images/logo.png" alt="tempton">
    </div>
    <div class="pg-page__section">
        <div class="pg-page__section-wrapper">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">BERUFSERFAHRUNG</h2>
            <?php component('table/table', ['tableOption' => 'Beruf / Firma Tätigkeit']); ?>
        </div>
    </div>
    <div class="pg-page__section">
        <h2 class="pg-headline pg-headline--color_turq" contenteditable="true">
            Alles weitere Wissenswerte zu unserem Besetzungsvorschlag finden Sie auf der nächsten Seite.
        </h2>
    </div>
    <div class="pg-page__section pg-page__section--footer">
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                200000_V1_11.02.2017
            </p>
        </div>
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                Alle personenbezogenen Bezeichungen stehen fur beide Geschlechter
            </p>
        </div>
        <div class="pg-page__footer-counter"></div>
    </div>
</div>

<div class="pg-page">
    <div class="pg-page__image-wrapper pg-page__image-wrapper--top">
        <img class="pg-page__image pg-page__image--logo" src="/images/logo.png" alt="tempton">
    </div>
    <div class="pg-page__section">
        <div class="pg-page__section-wrapper">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">BERUFSERFAHRUNG</h2>
            <?php component('table/table', ['simple' =>  true]); ?>
        </div>
    </div>

    <div class="pg-page__section">
        <div class="pg-page__section-wrapper">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">MOBILITÄT & SONSTIGES</h2>
        </div>
        <div class="pg-page__section-wrapper">
            <form class="pg-form">
                <label class="pg-form__label" contenteditable="true">Arbeitszeit:</label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="f_group">Vollzeit
                </label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="f_group">Teilzeit
                </label>
            </form>
            <form class="pg-form">
                <label class="pg-form__label" contenteditable="true">Schichtbereitschaft:</label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">ja
                </label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">eingeschränkt
                </label>
            </form>
            <form class="pg-form">
                <label class="pg-form__label" contenteditable="true">Reise- / Montagebereitschaft:</label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">ja
                </label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">nein
                </label>
            </form>
            <form class="pg-form">
                <label class="pg-form__label" contenteditable="true">Führerscheine:</label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">ja
                </label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">nein
                </label>
            </form>
            <form class="pg-form">
                <label class="pg-form__label" contenteditable="true">Eigener PKW:</label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">ja
                </label>
                <label class="pg-form__label pg-form__label--sm" contenteditable="true">
                    <input class="pg-form__input" type="checkbox" name="s_group">nein
                </label>
            </form>
        </div>
    </div>
    <div class="pg-page__section">
        <div class="pg-page__section-wrapper">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">KONDITIONEN</h2>
        </div>
        <div class="pg-page__section-wrapper pg-page__section-wrapper--row">
            <p class="pg-page__paragraph pg-paragraph pg-paragraph--color_black pg-paragraph--size_3" contenteditable="true">
                Datum
            </p>
            <p class="pg-paragraph pg-paragraph--bold pg-paragraph--color_black pg-paragraph--size_3" contenteditable="true">
                22.04.2016
            </p>
        </div>
        <div class="pg-page__section-wrapper">
            <p class="pg-paragraph pg-paragraph--color_black" contenteditable="true">
                Dieses Profil enthält vertrauliche Informationen und personenbezogene Daten.
                Ohne vorherige schriftliche Zustimmung durch TEMPTON Personaldienstleistungen GmbH dürfen diese Dritten nicht bekannt gegeben oder zugänglich gemacht werden. Mit Kontaktaufnahme zu der im Profil benannten Person, deren Vorstellung beim Kunden oder mit Ihrer Überlassung oder Einstellung in ein eigenes Arbeitsverhältnis beim Kunden, erkennt der Kunde diese an.
            </p>
        </div>
        <div class="pg-page__section-wrapper">
            <p class="pg-paragraph pg-paragraph--color_black" contenteditable="true">
                Es gelten die Bedingungen, welche Sie auf www.tempton.de in dem Menüpunkt „AGB“ unter der für die jeweils gewünschte Leistung passenden Bezeichnung finden. Auf Wunsch stellen wir Ihnen diese Bedingungen auch sehr gerne zur Verfügung.
            </p>
        </div>
    </div>
    <div class="pg-page__section">
        <div class="pg-page__section-wrapper">
            <h2 class="pg-headline pg-headline--color_turq pg-headline--uppercase" contenteditable="true">HABEN SIE AUCH INTERESSE AN ANDEREN TEMPTON-PRODUKTEN?
            </h2>
        </div>
        <div class="pg-page__section-wrapper">
            <?php component('list/list', [
                'listItems' => [
                    ['text' => 'Arbeitnehmerüberlassung', 'icon' => 'icon-sm'],
                    ['text' => 'Outsourcing', 'icon' => 'icon-sm2'],
                    ['text' => 'Technische Services', 'icon' => 'icon-sm3'],
                    ['text' => 'Personalübernahme', 'icon' => 'icon-sm4'],
                    ['text' => 'Personalabrechnung', 'icon' => 'icon-sm5'],
                    ['text' => 'Medical', 'icon' => 'icon-sm6'],
                    ['text' => 'Personalrekrutierung', 'icon' => 'icon-sm7'],
                    ['text' => 'Experten auf Zeit', 'icon' => 'icon-sm8'],
                    ['text' => 'Master Vendor', 'icon' => 'icon-sm9']
                ]

            ]); ?>
        </div>
        <div class="pg-page__section-wrapper">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_2" contenteditable="true">
                Dann informieren Sie sich gerne auf unserer Website unter <a href="www.tempton.de" class="pg-link pg-link--color_turq">www.tempton.de</a> oder rufen Sie uns für ein unverbindliches Informationsgespräch unter der Telefonnummer an.
            </p>
        </div>

    </div>
    <div class="pg-page__section pg-page__section--footer">
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                200000_V1_11.02.2017
            </p>
        </div>
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                Alle personenbezogenen Bezeichungen stehen fur beide Geschlechter
            </p>
        </div>
        <div class="pg-page__footer-counter"></div>
    </div>
</div>

<div class="pg-page pg-page--hidden">
    <div class="pg-page__image-wrapper pg-page__image-wrapper--width_30">
        <img class="pg-page__image" src="/images/Signature.png" alt="tempton">
    </div>
    <div class="pg-page__section pg-page__section--footer">
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                200000_V1_11.02.2017
            </p>
        </div>
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                Alle personenbezogenen Bezeichungen stehen fur beide Geschlechter
            </p>
        </div>
        <div class="pg-page__footer-counter"></div>
    </div>
</div>

<div class="pg-page pg-page--last">
    <div class="pg-page__image-wrapper">
        <img class="pg-page__image" src="/images/tempton-img-lastpage.jpg" alt="tempton">
    </div>
    <div class="pg-page__section pg-page__section--footer">
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                200000_V1_11.02.2017
            </p>
        </div>
        <div class="pg-page__footer-text">
            <p class="pg-paragraph pg-paragraph--color_black pg-paragraph--size_1" contenteditable="true">
                Alle personenbezogenen Bezeichungen stehen fur beide Geschlechter
            </p>
        </div>
        <div class="pg-page__footer-counter"></div>
    </div>
</div>