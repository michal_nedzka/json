<div class="pg-button-container">
    <div class="pg-button-container__wrapper">
        <?php component('button/button'); ?>

    </div>
    <div class="pg-button-container__wrapper pg-button-container__wrapper--padding_top-0">
        <p class="pg-paragraph pg-paragraph--size_2 pg-paragraph--color_black">
            Bitte beachten Sie, dass jegliche Änderungen nicht gespeichert werden und nach dem Schließen nicht wieder aufgerufen werden können
        </p>
    </div>
</div>