/*eslint-env node */
import util from 'gulp-util';
import browserSync from 'browser-sync';

import environment from '../environment.js';

/**
 * Watches all of the project files and notifies browserSync when needed.
 */
module.exports = function() {
    if ( util.env.sync ) {
        browserSync.exit();
        /*eslint global-require: 0*/
        browserSync.init( require( '../../browserSync.json' ) );
    }

    environment.watch = true;
    this.gulp.start( 'compile' );
};
