/*eslint-env node */
import del from 'del';

import path from '../path.js';

/**
 * Cleaning task settings.
 */
const settings = {
    /**
     * Paths that should be deleted.
     */
    src: [
        path.dist + '**/*',
        '.tmp'
    ]
};

/**
 * Task used for deleting old files in order to have a clean build.
 */
module.exports = function() {
    return del( settings.src );
};
