/*eslint-env node */
import browserSync from 'browser-sync';

import size from 'gulp-size';
import concat from 'gulp-concat';
import gulpif from 'gulp-if';
import uglify from 'gulp-uglify';

import path from '../../path.js';
import environment from '../../environment.js';

/**
 * Config for vendor JS files compilation task.
 */
const settings = {
    src: path.src + 'vendors/**/_*.js',
    dest: path.dist,
    filename: 'vendors.js'
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Compiles all files from given source path into single, minified bundle.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
    */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'scripts:vendors', browserSync.reload ] );
    }

    return this.gulp.src( settings.src )
        .pipe( size() )
        .pipe( concat( settings.filename ) )
        .pipe( gulpif( environment.production, uglify() ) )
        .pipe( this.gulp.dest( settings.dest ) );
};
