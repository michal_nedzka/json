/*eslint-env node */
import browserSync from 'browser-sync';
import size from 'gulp-size';
import gulpif from 'gulp-if';
import uglify from 'gulp-uglify';

import path from '../../path.js';
import environment from '../../environment.js';
/**
 * Config for standalone vendor files compilation.
 * By default all files that reside inside "vendors" directory and don't start
 * with "_" will be minified and copied to destination path.
 */
const settings = {
    src: path.src + 'vendors/**/!(_)*.js',
    dest: path.dist + 'vendors'
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Generates sourcemaps, minifies given files and copies them to destination path.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
    */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'scripts:standalone', browserSync.reload ] );
    }

    return this.gulp.src( settings.src )
        .pipe( gulpif( environment.production, uglify() ) )
        .pipe( size() )
        .pipe( this.gulp.dest( settings.dest ) );
};
