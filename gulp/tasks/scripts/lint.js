/*eslint-env node */
import gulpif from 'gulp-if';
import eslint from 'gulp-eslint';
import fs from 'fs';

import path from '../../path.js';
import environment from '../../environment.js';

/**
 * Settingf for JS linting task.
 * @type {Object}
 */
const settings = {
    src: [
        /**
         * Lint everything except vendors directory.
         */
        path.src + '!(vendors)/**/*.js'
    ],
    reportsDirectory: '_reports'
};

/**
 * Lints given files with eslint.
 */
module.exports = function() {
    if ( environment.jenkins ) {
        return this.gulp.src( settings.src )
            .pipe( eslint() )
            .pipe( eslint.format() )
            .pipe( eslint.format( 'checkstyle', fs.createWriteStream( settings.reportsDirectory + '/eslint-checkstyle.xml' ) ) )
            .pipe( eslint.failAfterError() );
    }

    return this.gulp.src( settings.src )
        .pipe( eslint() )
        .pipe( eslint.format() )
        .pipe( gulpif( environment.production, eslint.failAfterError() ) );
};
