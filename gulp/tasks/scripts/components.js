/*eslint-env node */
import rollupBabel from 'rollup-plugin-babel';
import rollupUglify from 'rollup-plugin-uglify';
import { rollup } from 'rollup';
import notifier from 'node-notifier';
import browserSync from 'browser-sync';

import path from '../../path.js';
import environment from '../../environment.js';

/**
 * Components JS compilation settings.
 */
const settings = {
    /**
     * JavaScript bundle entry point.
     */
    src: path.src,
    /**
     * JavaScript bundle destination filename.
     */
    filename: 'components.js',
    /**
     * Rollup configuration.
     * @see https://github.com/rollup/rollup/wiki/JavaScript-API#bundlegenerate-options-
     */
    rollup: {
        entry: path.src + 'components.js',
        plugins: []
    },
    bundle: {
        /**
         * JavaScript bundle destination directory.
         */
        dest: path.dist + 'components.js',
        format: 'iife',
        moduleName: 'yourJSNamespace',
        globals: {
            'jQuery': 'jQuery',
            'Modernizr': 'Modernizr',
            'svg4everybody': 'svg4everybody',
            'picturefill': 'picturefill'
        }
    }
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Task for compiling components' JS files.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
     */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src + '!(vendors)/**/_*.js',
            settings.src + settings.filename
        ], [ 'scripts:components', browserSync.reload ] );
    }

    settings.rollup.plugins = [
        rollupBabel( {
            exclude: [ 'node_modules/**', 'vendors/**' ],
            presets: [
                ["es2015", { "loose": true, "modules": false }]
            ],
            babelrc: false
        } )
    ];

    if ( environment.production ) {
        settings.rollup.plugins.push( rollupUglify() );
    }

    if ( environment.developement ) {
        settings.bundle.sourceMap = true;
    }

    return rollup( settings.rollup ).then( function( bundle ) {
        return bundle.write( settings.bundle );
    } ).catch( ( error ) => {
        notifier.notify( {
            'title': 'JS Error',
            'message': error.message
        } );

        return Promise.reject( error );
    } );
};
