/*eslint-env node */
import browserSync from 'browser-sync';

import gulpif from 'gulp-if';
import util from 'gulp-util';
import spritesmith from 'gulp.spritesmith';
import size from 'gulp-size';
import plumber from 'gulp-plumber';

import path from '../../path.js';
import environment from '../../environment.js';

/**
 * Settings for PNG sprites task.
 * @type {Object}
 */
const settings = {
    /**
     * Path to source files.
     */
    src: path.src + 'sprites/png/*.png',
    /**
     * Target directory for normal and retina sprites.
     */
    imgDest: path.dist + 'images/',
    /**
     * Target directory for SASS file.
     */
    cssDest: path.src + 'configs/',
    /**
     * Gulp.spritesmith settings.
     * @see https://github.com/twolfson/gulp.spritesmith#documentation
     */
    spritesmith: {
        retinaSrcFilter: path.src + 'sprites/png/*@2x.png',
        imgName: 'sprites.png',
        imgPath: '/images/sprites.png',
        retinaImgName: 'sprites2x.png',
        retinaImgPath: '/images/sprites2x.png',
        cssName: '_sprites.scss',

        cssVarMap: function( sprite ) {
            sprite.name = 'sprite-' + sprite.name;
        }
    }
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Generates sprites from all of the png images from given path using spritesmith plugin.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
    */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'sprites:png', browserSync.reload ] );
    }

    const sprite = this.gulp.src( settings.src )
        .pipe( gulpif( environment.development, plumber() ) )
        .pipe( size( { title: 'Images:', showFiles: util.env.verbose } ) )
        .pipe( spritesmith( settings.spritesmith ) );

    sprite.img
        .pipe( this.gulp.dest( settings.imgDest ) )
        .pipe( size( { title: 'Sprite:', showFiles: true } ) );

    return sprite.css
        .pipe( this.gulp.dest( settings.cssDest ) );
};
