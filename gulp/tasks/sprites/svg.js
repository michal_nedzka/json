/*eslint-env node */
import browserSync from 'browser-sync';

import gulpif from 'gulp-if';
import util from 'gulp-util';
import svgSprite from 'gulp-svg-sprite';
import size from 'gulp-size';
import plumber from 'gulp-plumber';

import path from '../../path.js';
import environment from '../../environment.js';

/**
 * Settings for SVG sprites.
 */
const settings = {
    src: path.src + 'sprites/svg/*.svg',
    dest: path.dist + 'images/',
    /**
     * Gulp-svg-sprite configuration.
     * @see https://github.com/jkphl/gulp-svg-sprite#api
     */
    svgSprite: {
        mode: {
            css: false,
            view: false,
            defs: {
                dest: '',
                sprite: 'sprites.svg'
            },
            symbol: false,
            stack: false
        }
    }
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Generates sprites from all of the svg images from given path.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
    */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'sprites:svg', browserSync.reload ] );
    }

    return this.gulp.src( settings.src )
        .pipe( gulpif( environment.development, plumber() ) )
        .pipe( size( { showFiles: util.env.verbose } ) )
        .pipe( svgSprite( settings.svgSprite ) )
        .pipe( size( { showFiles: util.env.verbose } ) )
        .pipe( this.gulp.dest( settings.dest ) );
};
