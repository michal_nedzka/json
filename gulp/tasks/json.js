/*eslint-env node */
import browserSync from 'browser-sync';

import util from 'gulp-util';
import size from 'gulp-size';

import path from '../path.js';
import environment from '../environment.js';

const settings = {
    src: path.src + '**/*.json',
    dest: path.dist
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Copies JSON files, nothing fancy.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
    */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'json', browserSync.reload ] );
    }

    return this.gulp.src( settings.src )
        .pipe( size( { showFiles: util.env.verbose } ) )
        .pipe( this.gulp.dest( settings.dest ) );
};
