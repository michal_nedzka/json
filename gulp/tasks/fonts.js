/*eslint-env node */
import browserSync from 'browser-sync';

import util from 'gulp-util';
import size from 'gulp-size';

import path from '../path.js';
import environment from '../environment.js';

/**
 * Settings for fonts task.
 */
const settings = {
    src: path.src + 'fonts/**',
    dest: path.dist + 'fonts/'
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Copies font files.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
    */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'fonts', browserSync.reload ] );
    }

    return this.gulp.src( settings.src )
        .pipe( size( { showFiles: util.env.verbose } ) )
        .pipe( this.gulp.dest( settings.dest ) );
};
