/*eslint-env node */
import browserSync from 'browser-sync';
import connectPhp from 'gulp-connect-php';

/**
 * Settings for serve task.
 */
const settings = {
    /**
     * Port for PHP server, must match port given in BrowserSync's proxy setting below.
     * @type {Number} Port number.
     */
    port: 8000,
    /**
     * BrowserSync configuration.
     */
    browserSync: {
        open: true,
        proxy: '127.0.0.1:8000'
    }
};

/**
 * Serves project files using PHP's build-in server on localhost address.
 * This function requires PHP installed and accesible using "php" command.
 */
module.exports = function() {
    let serverStarted = false;
    connectPhp.server( {
        base: 'dist',
        stdio: 'ignore',
        port: settings.port
    }, function() {
        /**
         * Somehow this callback was called twice.
         */
        if ( !serverStarted ) {
            serverStarted = true;
            /**
             * Wait a while for PHP server to start up.
             */
            setTimeout( function() {
                browserSync.exit();
                /*eslint no-sync: 0*/
                browserSync.init( settings.browserSync );
            }, 2000 );
        }
    } );
};

module.exports.dependencies = [ 'watch' ];
