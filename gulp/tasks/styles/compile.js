/*eslint-env node */
import sourcemaps from 'gulp-sourcemaps';
import notifier from 'node-notifier';
import gulpif from 'gulp-if';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import size from 'gulp-size';
import cleanCss from 'gulp-clean-css';
import browserSync from 'browser-sync';

import path from '../../path.js';
import environment from '../../environment.js';

/**
 * SASS compilation settings.
 */
const settings = {
    src: path.src + '**/*.{css,scss,sass}',
    dest: path.dist,
    autoprefixer: {
        browsers: [
            'ie >= 9',
            'ie_mob >= 10',
            'ff >= 30',
            'chrome >= 34',
            'safari >= 7',
            'opera >= 23',
            'ios >= 7',
            'android >= 4.2',
            'bb >= 10'
        ]
    },
    sass: {
        precision: 10,
        errLogToConsole: true
    }
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Compiles styles with SASS.
 * This function does following things:
 * - Generate sourcemaps,
 * - Compile with SASS,
 * - Autroprefix,
 * - Merge media queries,
 * - Minify with cleanCSS.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
     */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'styles:compile', browserSync.reload ] );
    }

    return this.gulp.src( settings.src )
        .pipe( gulpif( environment.development, sourcemaps.init() ) )
        /**
         * Prevent SASS from crashing watch on developement environment.
         */
        .pipe( gulpif( environment.development, sass( settings.sass )
            .on( 'error', sass.logError )
            .on( 'error', ( error ) => {
                notifier.notify( {
                    'title': 'SASS Error',
                    'message': error.messageOriginal
                } );
            } )
        ) )
        /**
         * Let it broke the build on production.
         */
        .pipe( gulpif( environment.production, sass( settings.sass ) ) )

        .pipe( autoprefixer( settings.autoprefixer ) )
        .pipe( gulpif( environment.production, cleanCss() ) )
        .pipe( size() )
        .pipe( gulpif( environment.development, sourcemaps.write() ) )
        .pipe( this.gulp.dest( settings.dest ) );
};
