/*eslint-env node */
import gulpif from 'gulp-if';
import sassLint from 'gulp-sass-lint';

import path from '../../path.js';
import environment from '../../environment.js';

/**
 * Settings for SASS linting task.
 */
const settings = {
    src: [
        /**
         * Lint everything inside components and layouts directories.
         */
        path.src + '{components,layouts}/**/*.s+(a|c)ss'
    ],
    reportsDirectory: '_reports'
};

/**
 * Lints given files with lint sass.
 */
module.exports = function() {
    return this.gulp.src( settings.src )
        .pipe( sassLint() )
        .pipe( sassLint.format() )
        .pipe( gulpif( environment.production, sassLint.failOnError() ) );
};
