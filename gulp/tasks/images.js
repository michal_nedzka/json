/*eslint-env node */
import browserSync from 'browser-sync';
import util from 'gulp-util';
import size from 'gulp-size';
import gulpif from 'gulp-if';
import imagemin from 'gulp-imagemin';

import path from '../path.js';
import environment from '../environment.js';

/**
 *  Configuration for images task.
 */
const settings = {
    src: path.src + 'images/**',
    dest: path.dist + 'images/',
    /**
     * Configuration for imagemin image minifier.
     * @see https://github.com/sindresorhus/gulp-imagemin#imageminoptions
     */
    imagemin: {
        progressive: true,
        interlaced: true
    }
};

/**
 * Variable used to fire gulp.watch only once.
 * @type {Boolean}
 */
let isWatching = false;

/**
 * Loads images from given path, minifies them with imagemin and
 * copies to given path.
 */
module.exports = function() {
    /**
     * If we are in watch mode, add watchers for this task.
    */
    if ( environment.watch && !isWatching ) {
        isWatching = true;

        this.gulp.watch( [
            settings.src
        ], [ 'images', browserSync.reload ] );
    }

    return this.gulp.src( settings.src )
        .pipe( gulpif( environment.production, imagemin( settings.imagemin ) ) )
        .pipe( size( { showFiles: util.env.verbose } ) )
        .pipe( this.gulp.dest( settings.dest ) );
};
