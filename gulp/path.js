/**
 * Default paths for a project.
 */
export default {
    /**
     * Path to sources directory relative to gulpfile.babel.js file.
     */
    src: 'src/',
    /**
     * Path to distribution directory relative to gulpfile.babel.js file.
     */
    dist: 'dist/'
};
