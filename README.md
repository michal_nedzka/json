# Project Boilerplate
## Project structure
General project structure looks as follows:
```
project-boilerplate/
├ src/
│  ├ commons/
|  ├ components/
|  ├ configs/
│  ├ fonts/
│  ├ images/
│  ├ layouts/
│  ├ sprites/
│  ├ utilities/
│  ├ vendors/
│  ├ base.scss
│  ├ components.scss
│  ├ components.js
│  └ index.php
├ dist/
├ gulp/
├ gulpfile.babel.js
├ browserSync.json.sample
└ packages.json
```

### `/src`
The main development section of the project. This folder contains all of the files that are used to build a functional static template.
The main idea for the sub-directiories was to separate files by the feature the provide. This way we ended up with following directories and files:

#### `/src/commons`
Contains files that are somehow shared across different parts of the project, e.g. base styles definition, general JS utilities etc.

#### `/src/components`
This directory contains all of the components used on the page. Each component MUST have its own directory with all of its files inside.
For more information see: ==== place link after merge ====

#### `/src/configs`
This directory contains all of the configuration for styles and JavaScript, e.g. SCSS variables, mixins, JSON configurations for JS etc.

#### `/src/fonts`
This directory contains all font files used across the pages.

#### `/src/images`
Contains all images that are used in the layouts but somehow cannot be merged into one sprite file. If you don't see any problems with using a certain image
as a sprite, put it inside `/src/sprites/png` directory instead.

#### `/src/layouts`
This folder MUST contain a separate folder for every template for the website e.g. homepage, checkout page, etc.
Any files specific for that certain layout(scripts, styles) MUST be placed in its directory and imported inside `/src/components.js` and `/src/components.*.sass` files.

#### `/src/utilities`
Contains `.php` that provide various functions that usually make authoring components more convenient.
For more information about available utilities see: ==== place link after merge ====

#### `/src/vendors`
Contains all third party scripts, styles and libraries that are used inside our project.
For more information see: ==== place link after merge ====

#### `/src/_setup.php`
Bootstrap file for every page template. For usage description see: ==== place link after merge ====

#### `/src/base.scss`
This file SHOULD contain all of the styles that are common for every template and device like: font definitions, typography etc.

#### `/src/components.*.scss`
This files SHOULD import styles of all of the components and layouts used on the website for a specific device that will be then merged into single file inside `/dist` directory.

#### `/src/components.js`
This files SHOULD import scripts of all of the components and layouts used on the website for a specific device that will be then merged into single file inside `/dist` directory.

#### `/src/index.php`
Entry point for our application. This file will usually contain sitemap to layouts.

### `/dist`
This directory contains only compiled sources and is generated automatically by gulp tasks.
This directory MUST contain all of the files needed for the backend team, so they don't have to dive into `/src` directory.
**All changes made here will be overwritten by gulp tasks!**

### `/gulp`
This directory contains all of the defined Gulp task functions that are then used inside `gulpfile.babel.js` file.

## Gulp
Gulp is our main build automation program responsible for magic tasks described bellow.

### Environments
We have two environments available: **development** (default) and **production**. To enable production environment just add `--env production` to your gulp task, fe.
```
gulp compile --env production
```
Production environment compiles and compress sources without sourcemaps.

### Compilation
```
gulp compile
```
Compiles entire project including SASS, JavaScript, images, sprites and templates.

#### SASS
Calling separately:
```
gulp compile:sass
```
Included in `gulp compile` task.

This task is responsible for compiling SASS to CSS (thanks cpt. Obvious). We are working using BEM methodology.
Every file which name does not start with `_` is a main file and will be compiled and copied into corresponding directory inside `/dist` e.g.
`/src/vendors/jquery.js` will be compiled and copied into `/dist/vendors/jquery.js`.
When the file name starts with a `_` it means that it's a partial meant for importing inside main files.

#### JavaScript
These tasks are responsible for managing JavaScript files in the project.
The main idea is the same as with SASS files: `_` means partial, without `_` means main file.

##### Components
Calling separately:
```
gulp compile:js:components
```
Included in `gulp compile` task.

This task is responsible for parsing `/src/components.js` file. With the use of `rollup` library, it follows all of the imports and creates optimal bundle.
Thanks to `babeljs` we are now able to author components using ES2015 syntax and features. Resulting file will be `/dist/compontents.js`.

##### Vendors
Calling separately:
```
gulp compile:js:vendors
```
Included in `gulp compile` task.

This task is responsible for taking every JS file inside `/src/vendors` directory that starts with `_` and bundle them into single `/dist/vendors.js` file.

##### Standalone
Calling separately:
```
gulp compile:js:standalone
```
Included in `gulp compile` task.

This task is responsible for taking every JS file inside `/src/vendors` directory that doesn't start with `_` and copy it into `/dist/vendors/`. This task
can be useful for libraries like _jQuery_ or _modernizr_ that should be included before other vendor scripts or ASAP.

### Sprites
Project has added automatic sprites generator for PNG's and SVG's.

#### PNG
Task does two magic things:
1. Generates both normal and retina sprites based on images placed in the `src/sprites/png/` directory (__only *.png files!__),
2. Delivers SASS mixins and variables in `src/configs/_sprites.scss`, where each image is represented by `$sprite-png-file-name-group` variable(`_` converted to `-`), e.g. `$sprite-logo`.

Notes:
* You must put both normal and retina version of images,
* Images must be named like `example.png` and `example@2x.png`,
* Retina image must be exactly 2x larger than normal version.

Sample usage:
```
.foo {
    @include retina-sprite($sprite-logo-group);
}
```
This code will generate needed CSS for proper normal and retina sprite usage as element's background.

#### SVG
This task works similar to its counterpart (generates sprite map image basing on `src/sprites/svg/` directory). The difference is that we have no SASS mixins and variables. Images are available as `#svg_file_name`.

Sample usage:
```
<svg>
    <use xlink:href="images/sprites.svg#logo"></use>
</svg>
```

### Templates
```
gulp templates
```
Copies all of the template files into corresponding paths inside `/dist` directory.

### Fonts
```
gulp fonts
```
Copies all of the font files from `/src/fonts` to `/dist/fonts`.

### Cleaning
```
gulp clean
```
This task erases contents of `/.tmp` and `/dist` directories so no conflicts occur between compilations.

## How to work
Open command terminal and go to the project directory.

### Requirements
Project workflow requires to have Node.js and Gulp>3.9.0 installed on your computer. If you want to use `gulp serve` task you will have to have PHP > 5.4 installed and globally accessible under `php` command.

#### Node.js
In the terminal type `node --version`. If there is no response, go to the [nodejs.org](https://nodejs.org/) and install it.

#### Gulp
Check in the console if you have already installed Gulp by typing `gulp --version`. If not, execute such command:
```
npm install --global gulp
```
Next step is to install our project dependencies. Execute:
```
npm install
```
### Tasks

#### Compile
```
gulp compile
```
This task delivers compiled sources of the project.

#### Watch
```
gulp watch
```
This task is responsible for infinite watching for changes in sources and compiling them when change is caught.

##### BrowserSync
If you already have a local server pointing to `/dist` directory then you can enable automatic browser refresh by typing `--sync` parameter to `gulp watch` command:
```
gulp watch --sync
```
This will run BrowserSync with existing `/browserSync.json` configuration file(sample provided).

#### Build-in server
```
gulp serve
```
This task allows to run a project without any external PHP server dependencies. It uses PHP's build in server and BrowserSync so you can rapidly develop the project without the need to install anything.

## Backend guide
Thanks to improvements over previous project structure it's now possible to separate work done by front- and back-end teams.
### Main guidelines
* All of the needed files MUST be present inside `/dist` folder after project compilation,
* If a certain file other than template is present in `/dist` or any subfolder then it is most probable that other scripts, styles etc. assume it will be
accessible under that path. E.g. `/dist/vendors/jquery.js`.
If any file that is outside `/dist` directory is required by backend team please inform us ASAP so we can resolve this issue.

## Frontend guide
### Component authoring
Usually components are made of 3 types of files: PHP templates, SASS styles and JS scripts.
#### PHP template
Below is the short boilerplate for a component template file:
```
<?php
set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
require '_setup.php';
?>
Your component HTML...
```
First line allows requiring files the same way on both local and build-in PHP server.
The second line imports `/_setup.php` file which is responsible for:
1. Telling PHP to output any PHP errors for better debugging,
2. Setting proper include path for utility functions,
3. Setting `$mobile`, `$tablet`, `$desktop`, `$device` variables for adaptive design,
4. Including all utility functions.

##### Utility functions
When authoring PHP, after including `/_setup.php` file, you are able to use following utility functions:
* `common` - includes PHP file from `commons` folder,
* `component` - includes PHP file from `components` folder,
* `layout` - includes PHP file form `layouts` folder.

Every function takes following arguments:
1. `$path: string` - path to the PHP file that should be included without extension and without `_` file prefix e.g. `foo/bar` will include `components/foo/_bar.php` file.
2. ``$arguments: array` - array of variable_name => variable_value, it will be extracted and available for included component e.g. `['foo' => 'bar']` will allow component to use variable `$foo` with value `'bar'`.

##### Device variables
Note: If you are not going to use an adaptive approach, you can skip this section.

When doing adaptive design it is required to be able to define device for which view we want to see.
First of all, you can define the device by placing appropriate GET parameter in page URL: `?tablet` or `?desktop`. You don't have to use `?mobile`, because it is the default type of view.
Now, you are able to access the type of the device that should be emulated by accessing `$mobile`, `$tablet` and `$desktop` variables which are defined as booleans e.g.
```php
if($tablet) {
    component('foo');
}
```
Above example shows the situation when the component is only visible on tablet devices, when you want to display a certain variation of a component you can use `$device` variable which contains the name of the device(mobile, tablet, desktop).
Say you have 3 different component files: `foo.mobile.php`, `foo.tablet.php` and `foo.desktop.php`, to include them dynamically you can write:
```php
component('foo.' . $device);
```
